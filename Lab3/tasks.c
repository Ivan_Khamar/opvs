#include "tasks.h"
#include "lightutils.h"
#include "osObjects.h"  
#include "cmsis_os.h"

#define MUL 1.5

osThreadId tid_redLEDTask;
osThreadId tid_blueLEDTask;
osThreadId tid_greenLEDTask;

void THREADtask_RedLight(void const *argument) {
		lightsOn(RED);
		osDelay(MUL * 1000);
		lightsOff(RED);
}

void THREADtask_Green_Blue_Orange_Light(void const *argument) {
	for(int i = 0; i < 10; i++) {
		if(i == 9)
			{
				lightsOn(GREEN);
				lightsOn(BLUE);
				lightsOn(ORANGE);
				//osDelay(MUL * 500);
			}
		//lightsOn(RED);
		
		//lightsOff(RED);
		//lightsOff(GREEN);
		//lightsOn(BLUE);
		//lightsOn(ORANGE);
		//osDelay(MUL * 1000);
	}
}