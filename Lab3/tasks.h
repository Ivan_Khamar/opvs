#ifndef TASKS_H
#define TASKS_H

#include "osObjects.h" 
#include "cmsis_os.h"

extern osThreadId tid_redLEDTask;
extern osThreadId tid_blueLEDTask;
extern osThreadId tid_greenLEDTask;

void THREADtask_RedLight(void const *argument);
void THREADtask_Green_Blue_Orange_Light(void const *argument);

#endif
