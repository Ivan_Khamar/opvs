#include "init.h"
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_hal_rcc.h"
#include "stm32f4xx_hal_cortex.h"
#include "tasks.h"
#include "osObjects.h"
#include "cmsis_os.h"

// func init
void EXTI0_IRQHandler(void);
void GPIO_Init(void);
void UserButton_Init(void);

void labInit() {
	__HAL_RCC_GPIOD_CLK_ENABLE();
	
	GPIO_Init();
}

void GPIO_Init() {
	// GPIO
	GPIO_InitTypeDef GPIO_InitStructure;
	
	GPIO_InitStructure.Pin = GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15;
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	
	HAL_GPIO_Init(GPIOD, &GPIO_InitStructure);
}

void UserButton_Init() {
	GPIO_InitTypeDef GPIO_InitStruct;
	
	GPIO_InitStruct.Pin = GPIO_PIN_0;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct); 
	
	HAL_NVIC_SetPriority(EXTI0_IRQn, 3, 0);
	HAL_NVIC_EnableIRQ(EXTI0_IRQn);
}

void EXTI0_IRQHandler(void) {
	osDelay(20);
	if(__HAL_GPIO_EXTI_GET_FLAG(GPIO_PIN_0)){
		int32_t signals = 0x00000001;
		osSignalSet (tid_greenLEDTask, signals);
		osDelay(1250);
		osSignalClear (tid_greenLEDTask, signals);
	}
 
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
}
