/*----------------------------------------------------------------------------
 * CMSIS-RTOS 'main' function template
 *---------------------------------------------------------------------------*/

#define osObjectsPublic                     // define objects in main module
#include "osObjects.h"                      // RTOS object definitions
#include "cmsis_os.h"
#include "init.h"
#include "tasks.h"

osThreadDef(THREADtask_RedLight, osPriorityNormal,1,0);
osThreadDef(THREADtask_Green_Blue_Orange_Light, osPriorityNormal,1,0);

/*
 * main: enter point for initialization and program start
 */
int main (void) {
	// CMSIS-RTOS initialization
	osKernelInitialize(); 
	
	labInit();
	
	// threads:
	tid_redLEDTask = osThreadCreate(osThread(THREADtask_Green_Blue_Orange_Light), NULL);
	osDelay(600);
	tid_blueLEDTask = osThreadCreate(osThread(THREADtask_RedLight), NULL);
	osDelay(600);
	
	// Thread execution begin
  osKernelStart();      
}
